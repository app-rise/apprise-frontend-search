import { Styled, classname } from 'apprise-ui/component/model'
import partition from 'lodash/partition'
import { PropsWithChildren } from 'react'
import { useSearchProfile } from './config'
import './querypanel.scss'
import { QueryPillProps, useQueryPills } from './querypill'
import { OpenSidebarButton, QueryResetButton, QueryRevertButton, SaveQueryButton, SearchButton } from './searchactions'
import { PageConfig } from './searchpage'


export const QueryPanelConfig = (_: Partial<QueryPanelProps>) => null


export const QueryAction = (props: PropsWithChildren<Partial<{ orientation: 'left' | 'right' }>>) => props.children





export type QueryPanelProps = PropsWithChildren<Styled & {

    filterPills: (p: Partial<QueryPillProps>) => boolean

}>


export const QueryPanel = (props: Partial<QueryPanelProps>) => {

    const { panel, page = {}, actions } = useSearchProfile().props('panel', QueryPanelConfig).all('actions', QueryAction).props('page', PageConfig).get()

    const allprops = { ...panel, ...props }

    const { className, style } = allprops

    const pills = useQueryPills(allprops)

    const { SelectedPill, PillButton, TabButton } = pills

    const [leftActions, rightActions] = partition(actions, a => ((a.props as Parameters<typeof QueryAction>[0]).orientation ?? 'left') === 'left')

    return <div className={classname('query-panel', className)} style={style}>

        <div className='tab-btns'>
            {pills.tabs.length>1 && pills.tabs.map(tab =>

                <TabButton key={tab} name={tab} />

            )}
        </div>

        <div className='pills-btns'>
            {pills.all.map(props =>

                <PillButton key={props.condition} {...props} />

            )}
        </div>


        <div className='pills-selected'>
            {pills.selected.map((props, i) =>

                <SelectedPill key={i} {...props} />

            )}
        </div>

        <div className='form-controls'>

            {leftActions}
            <QueryRevertButton />
            <QueryResetButton />
            <SearchButton />
            <SaveQueryButton />
            <OpenSidebarButton startOpen={page.defaultOpen} />
            {rightActions}

        </div>

        {panel.children &&

            <div className='panel-footer'>
                {panel.children}
            </div>
        }
    </div>
}

