import { useT } from 'apprise-frontend-core/intl/language'
import { Crumb } from 'apprise-ui/scaffold/scaffold'
import { searchRoute } from './router'


export const useSearchCrumbs = () => {

    const t = useT()
    
    return <>
        <Crumb path={searchRoute}>{t('search.singular')}</Crumb>
    </>

}
