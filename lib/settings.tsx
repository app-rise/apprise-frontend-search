import { useT } from 'apprise-frontend-core/intl/language'
import { SettingRenderProps, SettingsModule } from 'apprise-frontend-streams/settings/model'
import { Form } from 'apprise-ui/form/form'
import { NumberBox } from 'apprise-ui/numberbox/numberbox'
import { SearchIcon } from './const.ui'
import { defaultDownloadPageLimit, defaultPageSize, searchType } from './const'


export type SearchSettings = {

    pageSize: number
    downloadPageLimit: number
}


const useSettingsFields = (_:SearchSettings) => {

    const t = useT()

    return {

        pageSize: {

            label: t('search.pagesize_lbl'),
            msg: t('search.pagesize_msg'),
            help: t('search.pagesize_help')
        },

        downloadPageLimit: {

            label: t('search.pagelimit_lbl'),
            msg: t('search.pagelimit_msg'),
            help: t('search.pagelimit_help')
        },
    }
}

export const SearchSettingsPanel = (props: SettingRenderProps<SearchSettings>) => {


    const { onChange, settings } = props

    const fields = useSettingsFields(settings)


    return <Form>

        <NumberBox info={fields.pageSize} min={50} width={100} onChange={size => onChange({ ...settings, pageSize: size ?? defaultPageSize})}>
            {settings.pageSize}
        </NumberBox>

        <NumberBox info={fields.downloadPageLimit} min={5} width={100} onChange={limit => onChange({ ...settings, downloadPageLimit:  limit ?? defaultDownloadPageLimit})}>
            {settings.downloadPageLimit}
        </NumberBox>

    </Form>
}

export const useSearchSettingsModule = () : SettingsModule<SearchSettings> => {

    return {

        Icon: SearchIcon,
        name: 'search.settings_name',
        defaults: {
            pageSize: defaultPageSize,
            downloadPageLimit: defaultDownloadPageLimit
        },
        type: searchType,
        Render: SearchSettingsPanel,
        fields: useSettingsFields

    }
}