import { useCall } from 'apprise-frontend-core/client/call'
import { usePreload } from 'apprise-frontend-core/client/preload'
import { useLanguage } from 'apprise-frontend-core/intl/language'
import { searchApi, SearchDto } from './api'
import { defaultPageSize } from './const'
import { Query, Result, ResultBatch, SearchRequest } from './model'



export const useSearchCalls = () => {

    const currentLanguage = useLanguage().current()

    const call = useCall()
    
    const preload = usePreload()

    const extern = (req: Partial<SearchRequest>) : SearchDto => {

        const { query, language= currentLanguage, total, cursor = { page: 1, pageSize: defaultPageSize}} = req
     
        return  {
     
             conditions: Object.values(query?.conditions ?? {}).flat().map(q => q.data),
             sort: query?.sort?.map(s => ({ field: s.key as string, mode: s.mode! })) ?? [],
             modes: query?.modes ?? [],
             language,
             cursor,
             total,  // hint to stateless backend not to recount for better perf.
         }
     
                 
     }

    const self = {


        search: async <Q extends Query, R extends Result = Result>(request: Partial<SearchRequest<Q>>, noPreload?: boolean) => {

            const run = () => {

                const dto = extern(request)

                console.log("searching with:", dto)
               
                return call.at(searchApi).post<ResultBatch<R>>(dto)

            }

            return noPreload ? run() : preload.get<ResultBatch<R>>(searchApi) ?? run()
        }

    }

    return self
}
