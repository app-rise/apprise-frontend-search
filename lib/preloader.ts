import { Preloader } from 'apprise-frontend-core/client/preload'
import { useSearchCalls } from './calls'
import { SearchRequest } from './model'
import { searchApi } from './api'


// a preloader to register on <Preload> as a render-time optimisation.
// applications can and should optimise further by inlining their start query in index.html
export const useSearchPreloaders = (props: Partial<SearchRequest>): Preloader[] => {

    const call = useSearchCalls()

    return [{ name: searchApi, task: () => call.search(props) }]

}