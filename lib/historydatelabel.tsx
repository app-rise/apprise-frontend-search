
import { Label, LabelProps } from 'apprise-ui/label/label'
import { DateIcon } from 'apprise-ui/utils/icons'
import { useDateFnsLocale } from 'apprise-ui/utils/localeloader'
import format from 'date-fns/format'
import isToday from 'date-fns/isToday'

export type DateLabelProps = LabelProps & Partial<{

    date: number | string

    dateFormat: string
}>

export const DefaultHistoryDateLabel = (props: DateLabelProps) => {

    const { date: clientdate, dateFormat='HH:mm:ss', icon, ...rest } = props

    const date = typeof clientdate === 'string' ? Date.parse(clientdate) : clientdate

    const locale = useDateFnsLocale()

    if (!date)
        return null

    
    const formatted = format(date, dateFormat, { locale })

    const fullFormat = isToday(date) ? formatted : `${formatted} (${format(date, 'dd/mm', {  locale })})`


    return <Label tip={format(date, 'pp')} icon={icon ?? <DateIcon />} title={fullFormat} {...rest} />
}
