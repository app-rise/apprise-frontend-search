import { useT } from 'apprise-frontend-core/intl/language'
import { Button, ButtonProps } from 'apprise-ui/button/button'
import { ChoiceBox } from 'apprise-ui/choicebox/choicebox'
import { classname } from 'apprise-ui/component/model'
import { Form } from 'apprise-ui/form/form'
import { SliderBox } from 'apprise-ui/sliderbox/slider'
import { useAsyncTask } from 'apprise-ui/utils/asynctask'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { useState } from 'react'
import { ImDownload } from 'react-icons/im'

import { useStable } from 'apprise-frontend-core/utils/function'
import { useSettings } from 'apprise-frontend-streams/settings/api'
import { useDrawer } from 'apprise-ui/drawer/drawer'
import { DownloadIcon } from 'apprise-ui/utils/icons'
import { useSearchContext } from './boundary'
import { useSearchCalls } from './calls'
import { useSearchProfile } from './config'
import { searchType } from './const'
import { useCurrentQuery } from './context'
import './downloadpanel.scss'
import { useSearchEffects } from './searcheffects'
import { SearchSettings } from './settings'
import { Result } from './model'




export type DownloadMode = 'codes' | 'names' | 'both'

export type ResultWriter <R extends Result=Result> =  (_: R[], __ : DownloadMode) => Promise<void>



export const DownloadConfig = <R extends Result>(_: {

    disable?: boolean
    maxDownloadSize?: number
    writer: ResultWriter<R>

}) => null


export const useResultDownload = () => {

    const t = useT()

    const { config } = useSearchProfile().props('config', DownloadConfig).get()

    const { Drawer, openSet: downloadOpenSet } = useDrawer()


    const Button = useStable((props:ButtonProps) =>  config?.disable ? null : <DownloadButton onClick={() => downloadOpenSet(true)} {...props} /> )

    const Panel = useStable(()=> {
       
        if (config.disable)
            return null

        const { writer } = config ?? {}

        return <Drawer title={t('search.download_title')} icon={<DownloadIcon />} unmountOnClose >
            <DownloadPanel writer={writer} />
        </Drawer>
        
    })


    return { DownloadPanel : Panel, DownloadButton: Button  }
}

export const DownloadButton = (props:ButtonProps) => {
    
    const t = useT()

    const query = useCurrentQuery()

    return <Button type='primary' disabled={query?.dirty} icon={<DownloadIcon />} {...props}>
        {t('search.download_title')}
    </Button>
}

export const DownloadPanel = (props: {

    writer: ResultWriter

}) => {

    const t = useT()

    const { writer: write } = props

    const task = useAsyncTask()

    const calls = useSearchCalls()

    const { makeRequest } = useSearchEffects()

    const state = useSearchContext()

    const { pageSize, downloadPageLimit } = useSettings<SearchSettings>(searchType)

    const { total } = state.get()

    const pages = Math.ceil(total / pageSize)
    const maxPages = downloadPageLimit
    const maxDownloadSize = downloadPageLimit * pageSize

    const tooManyResults = total > maxDownloadSize
    const defaultScope = tooManyResults ? 'current' : 'all'

    const [scope, scopeSet] = useState<'current' | 'all' | 'range'>(defaultScope)

    const [pagerange, pagerangeSet] = useState([1, maxPages] as [number, number])

    const [mode, modeSet] = useState<'codes' | 'names'>('codes')

    const [firstPage, lastPage] = pagerange


    const tooManyPages = scope === 'range' && (lastPage - firstPage >= maxPages)

    const marksArray = [1, firstPage, lastPage, pages]

    const marks = marksArray.filter((n, i) => marksArray.indexOf(n) === i).reduce((acc, next) => ({ ...acc, [next]: <span>{next}</span> }), {})

    const download = task.make(async () => {

        const cursor = { offset: (firstPage - 1) * pageSize, limit: (lastPage - (firstPage - 1)) * pageSize }

        const { results } = scope === 'current' ? state.get() : await calls.search({ ...makeRequest(), cursor })

        await write(results, mode)

    }).with($ => $.show(t("search.downloading"))).done()

    return <ExplainerPanel className='download-panel'>

        <Explainer icon={ImDownload}>
            {t("search.download_explainer")}
            <br />
            {t("search.download_explainer2", { max: maxDownloadSize })}
        </Explainer>

        <Form centered>

            <ChoiceBox radio value={scope} onChange={v => scopeSet(v!)}>
                <ChoiceBox.Option disabled={tooManyResults} value='all' title={t('search.download_all')} />
                <ChoiceBox.Option disabled={pages < 2} value='current' title={t('search.download_current')} />
                <ChoiceBox.Option disabled={pages < 2} value='range' title={t('search.download_range')} />
            </ChoiceBox>

            <SliderBox.Interval label={t('search.download_range_lbl')} centered className={classname('page-range', scope === 'range' && 'range-active')}
                status={tooManyPages ? 'error' : 'success'}
                msg={tooManyPages ? t('search.download_range_error', { max: maxPages }) : undefined}
                marks={marks}
                min={1}
                max={pages}
                onChange={range => range && pagerangeSet(range)}>
                {pagerange}
            </SliderBox.Interval>

            <br />
        </Form>

        <Explainer>
            {t("search.download_explainer3")}
        </Explainer>

        <Form centered>

            <ChoiceBox radio value={mode} onChange={v => modeSet(v ?? 'codes')}>
                <ChoiceBox.Option value='codes' title={t('search.download_codes')} />
                <ChoiceBox.Option value='names' title={t('search.download_names')} />
                <ChoiceBox.Option value='both' title={t('search.download_codes_and_names')} />
            </ChoiceBox>

        </Form>

        <Button className='download-btn' noReadonly type="primary" disabled={tooManyPages} onClick={download}>
            {t('search.download_btn')}
        </Button>



    </ExplainerPanel>

}

