import { PreferenceUpdater, useUIPreferences } from 'apprise-ui/preferences';
import { SavedQuery } from './model';


export type SearchPreferences = {

    columns: React.Key[],

    queries: SavedQuery[]

}

export type SearchPreferenceHandler = {

    get: () => SearchPreferences

    set: (_: PreferenceUpdater<SearchPreferences>) => void

}

export type UISearchPreferences = {

    search: SearchPreferences

}

const startPreferences : SearchPreferences = {

    queries: [],
    columns: []
}

export const useDefaultSearchPreferences = () : SearchPreferenceHandler => {

    const preferences = useUIPreferences()

    return  {

        get: () => preferences.get().search ?? startPreferences,

        set: updater => preferences.set( current  =>  {

            if (!current.search)
                current.search = startPreferences
         
            updater(current.search )
        
        
        })
        

    }

}