import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { Button } from 'apprise-ui/button/button'
import { Label } from 'apprise-ui/label/label'
import { SidebarContent } from 'apprise-ui/page/sidebar'
import { Table } from 'apprise-ui/table/table'
import { useTableUtils } from 'apprise-ui/table/utils'
import { useFeedback } from 'apprise-ui/utils/feedback'
import { RemoveIcon, TextIcon } from 'apprise-ui/utils/icons'
import { useMemo } from 'react'
import { useSearchContext } from './boundary'
import { SearchIcon } from './const.ui'
import { useCurrentQuery } from './context'
import { Query, SavedQuery } from './model'
import "./savedsearches.scss"
import { useSavedQuery } from './savequery'
import { useSearchPreferences } from './utils'




export const SavedSearches = () => {

    const t = useT()

    const fb = useFeedback()

    const state = useSearchContext()

    const preferences = useSearchPreferences()
    
    const { queries=[] } = preferences.get()

    const {dirty, edited} = useCurrentQuery()

    const drawer = useSavedQuery()

    type SavedQueryRow = SavedQuery & {

        disabled: boolean
        highlighted: boolean
    }

    const queryRows: SavedQueryRow[] = useMemo(() =>

        queries.map(query => ({

            disabled: dirty,


            highlighted: utils().deepequals(edited, query.query)

            ,

            ...query
        }))


        , [dirty, queries, edited])

    const { Column } = useTableUtils<SavedQueryRow>()

    
    const select = (query: Query) => () => state.set(s => s.query=query)

    const edit =  (query: SavedQuery) => () =>  drawer.openWith(query)
    const remove =  (query: SavedQuery) => () => fb.askRemovalConsent(t('search.singular').toLowerCase()).thenRun(() => preferences.set(prefs =>

        prefs.queries = queries.filter(q => q.name !== query.name)

    ))

    return <div className='saved-searches'>

        <SidebarContent.Title>{t('search.save_queries_title')}</SidebarContent.Title>

        <Table.Sider<SavedQuery> name='queries' fixedHeight noFilter emptyPlaceholder={t("search.save_queries_placeholder")} rowId={i => i.name} data={queryRows} mountDelay={0} >

            <Column render={row => {

                const { name, query, highlighted } = row

                const content = <Label icon={<SearchIcon />} title={name} highlighted={highlighted} />

                return <Button tip={t('search.saved_queries_load_tip')} disabled={highlighted}  onClick={select(query)}>
                    {content}
                </Button>

            }} />



            <Column width={40} align='right' render={query =>

                <div className='apprise-row'>
                    <Button noLabel icon={<TextIcon color='lightseagreen' />} tip={t('search.saved_queries_edit_tip')} className='search-action' disabled={query.disabled} onClick={edit(query)} />
                    &nbsp;
                    <Button noLabel icon={<RemoveIcon color='orangered' />}
                        tip={t('search.saved_queries_remove_tip')} className='search-action' 
                        disabled={query.disabled} onClick={remove(query)} />
                </div>

            } />
    

        </Table.Sider> 

    </div>

}