import { useLanguage } from 'apprise-frontend-core/intl/language'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { PropsWithChildren, useEffect } from 'react'
import { useSearchContext } from './boundary'
import { DefaultSearchContext, ResultCacheContext, SearchContextType, SearchState, initialSearchState } from './context'
import { Query } from './model'
import { useSearchEffects } from './searcheffects'
import { SearchPreferenceHandler } from './preferences'


type SearchProps = {

    boundary: SearchContextType     // the boundary associated with this context, if any.

    useStartQuery: () => Partial<Query>      // the query to run at the start in place of the default one.

    noStartRun: boolean             // if we should run a query at the start at all.

    usePreferenceHandler: () => SearchPreferenceHandler     // replaces the default handler of per-user search preferences.
}


export const Search = (props: PropsWithChildren<Partial<SearchProps>>) => {

    const { children, boundary = DefaultSearchContext, useStartQuery, usePreferenceHandler } = props

    const initial: SearchState = {
        ...initialSearchState,

        startQuery: { ...initialSearchState.startQuery, ...useStartQuery?.() },
        usePreferenceHandler: usePreferenceHandler ?? initialSearchState.usePreferenceHandler


    }

    return <StateProvider initialState={initial} context={boundary}>
        <LanguageChangeObserver />
        <StateProvider initialState={{}} context={ResultCacheContext}>
            <Initialiser {...props} />
            {children}
        </StateProvider>
    </StateProvider>

}



const LanguageChangeObserver = () => {

    const language = useLanguage().current()

    const search = useSearchContext()

    const effects = useSearchEffects()

    // re-runs query on lang change if we have sorted results that may be impacted by it.
    useEffect(() => {

        if (search.get().query?.sort?.length > 0)
            effects.runSearch()

        // eslint-disable-next-line
    }, [language])

    return null
}

// runs the start query on mount.
// applications should preloade results sooner than this (preloaders or inlined in index.html)
// then the query is still processed and pushed to history, but the network isn't involved.
const Initialiser = (props: Partial<SearchProps>) => {

    const { noStartRun } = props

    const effects = useSearchEffects()

    const search = useSearchContext()

    useEffect(() => {

        if (noStartRun)
            return

        effects.runSearch({ query: search.get().startQuery, source: 'system' })

        // eslint-disable-next-line
    }, [])

    return null
}
