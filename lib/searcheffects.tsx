import { useLanguage, useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { SortSpec } from 'apprise-ui/table/sorting'
import { useAsyncTask } from 'apprise-ui/utils/asynctask'
import { unstable_batchedUpdates } from 'react-dom'
import { useSearchContext } from './boundary'
import { useSearchCalls } from './calls'
import { Query, Result, SearchRequest } from './model'
import { useSettings } from 'apprise-frontend-streams/settings/api'
import { SearchSettings } from './settings'
import { searchType } from './const'


export const useSearchEffects = <R extends Result = Result, Q extends Query = Query>() => {

    const t = useT()

    const language = useLanguage().current()

    const task = useAsyncTask()

    const state = useSearchContext<Q, R>()

    const calls = useSearchCalls()

    const { pageSize } = useSettings<SearchSettings>(searchType)

    const runTask = task.make(async (request: SearchRequest<Q>) => {

        const { results, total } = await calls.search<Q, R>(request)

        state.set(s => {

            s.results = results
            s.total = total
        })


    }).with($ =>

        $.show(t('search.loading'))
        .throw({

            title: t('search.search_error_title'),
            message: t('search.search_error_msg')

        })
            .minimumDuration(300)

    ).done()



    const self = {


        // derives dto from state and overlays overrides, if any.
        makeRequest: (): SearchRequest<Q> => {

            const { page, query, total } = state.get()

            return {

                query,
                language,
                cursor: { page, pageSize },
                total,  // hint to stateless backend not to recount for better perf.
            }

        }

        ,

        changePage: (page: number) => {

            const currentPage = state.get().page

            if (page !== currentPage) {

                unstable_batchedUpdates(async () => {

                    const request = self.makeRequest()

                    request.cursor = {...request.cursor,page}

                    await runTask(request)

                    state.set(s => s.page = page)

                })


            }

        }

        ,

        changeSort: async (sort: SortSpec[]) => {

            const request = self.makeRequest()

            request.query = {...request.query, sort }

            await unstable_batchedUpdates(async () => {
              
                await runTask(request)

                state.set(s => s.query.sort = sort)
              
            })

        }

        ,

        // runs a search request based on the current state, optionally with overrides.
        // if the query is overridden, the new query is pushed to history and pagination is reset.
        // otherwise the history is updated with fresh timestamps.
        runSearch: async (request: Partial<SearchRequest<Q>> = {}) => {

            const mergedRequest = { ...self.makeRequest(), ...request }

            const { query, source, silent } = mergedRequest

            const { query: currentQuery, history } = state.get()

            const samequery = utils().deepequals(query, history[0]?.query ?? currentQuery)

            if (!samequery) 
                delete mergedRequest.total

            unstable_batchedUpdates(async () => {

                await runTask(mergedRequest)

                state.set(s => {

                    
                    if (samequery && !silent)  // update timestamp only.
                        s.history[0].timestamp = Date.now()

                    else {  // store quey and push on history.

                        s.query = query
                        silent || s.history.unshift({ timestamp: Date.now(), source, query, total: s.total })
                        s.page = 1

                    }
                })

            })

        }

    }

    return self
}