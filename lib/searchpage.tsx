import { StateProvider } from 'apprise-frontend-core/state/provider'
import { utils } from 'apprise-frontend-core/utils/common'
import { Page, PageProps } from 'apprise-ui/page/page'
import { SidebarContent, SidebarProps } from 'apprise-ui/page/sidebar'
import { SortSpec } from 'apprise-ui/table/sorting'
import { FC, Fragment } from 'react'
import { SearchBoundary, SearchBoundaryProps } from './boundary'
import { ConfigurationProfileProvider, ConfigurationProfileProviderProps, useSearchProfile } from './config'
import { SavedQueryContext, initialSavedQueryState } from './context'
import { useResultDownload } from './downloadpanel'
import { QueryProvider } from './queryprovider'
import { SavedSearches } from './savedsearches'
import { SavePanel } from './savequery'
import { HistoryProps, SearchHistory } from './searchhistory'
import './searchpage.scss'
import { SearchPanel } from './searchpanel'


export const PageConfig = (_: Partial<SearchPageProps>) => null

export const ResultDetail = (_: {

    component: FC

}) => null

export const SidebarConfig = (props: Partial<SidebarProps & HistoryProps & {

    noHistory: boolean

}>) => props.children



export type SearchPageProps = PageProps & SearchBoundaryProps & ConfigurationProfileProviderProps & {

    initialSort: SortSpec[]

}

// renders a page for search, with either custom children or a default <SearchPanel>.
// reners also a sidebar with search history, saved searches, and a download trigger.
// wraps around main content and sidebar:
// 1 a <SearchConfigurationProvider>, to shate witb all children the config in a <SearchProfile> below. 
// 2. an optional <SearchBoundary>, to connect with a specific search context.

export const SearchPage = (props: Partial<SearchPageProps>) => {

    // extracts profile optimisatons
    const { page, detail, sidebar, sidebarProps } = useSearchProfile()
        .props('page', PageConfig)
        .props('detail', ResultDetail)
        .one('sidebar', SidebarConfig)      // need  element
        .props('sidebarProps', SidebarConfig) // need its props too. 
        .get()

    const { noHistory, dateProps, dateLabel, ...restSidebar } = sidebarProps ?? {}

    // by default, assumes loading states remain local (cf. SearchPanel)
    const { children, noIndicator = true, ...rest } = { ...page, ...props }

    // custom content may contain 
    const primaryBtn = utils().elementsIn(sidebarProps.children).find(e => e.props.type === 'primary')

    const { DownloadPanel, DownloadButton } = useResultDownload()

    const Detail = detail?.component ?? Fragment

    const hasChildren = utils().elementsIn(props.children).length

    return <SearchBoundary {...props}>

        <ConfigurationProfileProvider>

            <QueryProvider>

                <StateProvider context={SavedQueryContext} initialState={initialSavedQueryState}>

                    <Page className='search-page' noIndicator={noIndicator} {...rest}>

                        <SidebarContent {...restSidebar}>
                            <DownloadButton type={primaryBtn ? 'normal' : 'primary'} />

                            {sidebar && <br />}
                            {sidebar}
                            {sidebar && <br />}

                            <div className='search-siders'>
                                <div style={{ flexGrow: 2 }}>
                                    <SavedSearches />
                                </div>

                                {noHistory ||

                                    <div style={{ flexGrow: 1 }}>
                                        <SearchHistory dateProps={dateProps} dateLabel={dateLabel} />
                                    </div>}
                            </div>

                        </SidebarContent>

                        {children}

                        {
                            //returns a standard panel, unless the application takes over with specific children
                            hasChildren || <SearchPanel />

                        }

                        <Detail />

                        <SavePanel />
                        <DownloadPanel />

                    </Page>

                </StateProvider>

            </QueryProvider>

        </ConfigurationProfileProvider>

    </SearchBoundary>


}