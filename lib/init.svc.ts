import { utils } from 'apprise-backend-helper/core/common'
import { QueryFragment } from 'apprise-backend-helper/db/utils'
import { Query, SearchConditionData } from 'apprise-frontend-search/model'
import postgres from 'postgres'
import { SearchDto } from './api'
import { initSearchApi } from './api.svc'



export type SearchConfig = {


    schema: string
    table: string
    jsoncol: string,


    requestHandler: RequestHandler
    conditionHandlers: Record<string,ConditionHandler>
    sortHandlers: Record<string,SortSpecHandler>


}

export type RequestHandler = (data: SearchDto) => SearchDto

export type ConditionHandler <T extends SearchConditionData = SearchConditionData> = (data: T) => { 
    
    whereFragment: QueryFragment
    joinFragment?: QueryFragment
}

export type SortSpecHandler = (dto:SearchDto) => {

    colExpression: QueryFragment | postgres.Helper<any>
    joinFragment?: QueryFragment

}



export const defaultSearchConfig: SearchConfig = {

    schema: 'public',
    table: 'record',
    jsoncol: 'details',

    requestHandler: dto =>dto,
    conditionHandlers: {},
    sortHandlers: {}
}


export const useStartQuery = () : Partial<Query> => {


    return {
        conditions: {}
    }

}


let searchConfig: SearchConfig


export const getSearchConfig = () => searchConfig ?? defaultSearchConfig


export const initSearch = (config: Partial<SearchConfig>={}) => {

    // suplements defaults
    const defaultedSearchConfig = utils().merge(defaultSearchConfig, config)

    // // combines, post-process, etc.
    const postProcessedConfig = {

        ...defaultedSearchConfig,

        table: `${defaultedSearchConfig.schema}.${defaultedSearchConfig.table}`
    } 

    searchConfig = postProcessedConfig


     initSearchApi()


}