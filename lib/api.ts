

import { Language } from 'apprise-frontend-core/intl/language.js'
import { SearchConditionData, SearchCursor, Sort } from './model.js'


export const searchApi = `/search`

export type SearchDto = {

    modes: string[], 
    conditions: SearchConditionData[]
    sort: Sort[]
    cursor: SearchCursor
    language: Language


    total?: number      // set only after query is first run.
}



