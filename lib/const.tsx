
import { Query } from './model';

export const searchType = 'search'


export const defaultDownloadPageLimit = 10

export const defaultPageSize = 100

export const defaultStartQuery: Query =  {

    conditions: {},
    modes:[],
    sort: []
}

