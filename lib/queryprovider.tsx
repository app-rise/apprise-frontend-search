
import { useForm } from 'apprise-frontend-core/utils/form'
import { PropsWithChildren } from 'react'
import { useSearchContext } from './boundary'
import { QueryContext } from './context'

// shares the query under editing with children, starting from the query in context, if any exists, or the start query.

export const QueryProvider = (props: PropsWithChildren) => {

    const state = useSearchContext()

    const { children } = props

    const query = state.get().query ?? state.get().startQuery

    const form = useForm(query)

 
    return <QueryContext.Provider value={form}>
        {children}
    </QueryContext.Provider>

}
