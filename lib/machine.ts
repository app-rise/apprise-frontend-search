import { useMachine } from 'apprise-frontend-core/utils/machine'
import { useSearchEffects } from './searcheffects'

export type SearchMachineState = 'idle' | 'running'
export type SearchMachineEvent = 'run' 

export const useSearchsMachine = () => {

    const effects = useSearchEffects()

    const machine = useMachine<SearchMachineState, SearchMachineEvent>((allow, { trigger, transitionTo }) =>

        allow
            .from('idle').to('running').on('run').and(()=>effects.runSearch().then(transitionTo('idle')))
            .from('running').to('idle')

            .startFrom('idle').and(trigger('run'))

        , { debug: false })


    return machine
}