import { useSearchContext } from './boundary';




export const useSearchPreferences = () => {

    const {  usePreferenceHandler } = useSearchContext().get()

    return usePreferenceHandler()
}