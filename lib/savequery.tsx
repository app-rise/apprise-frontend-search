import { useT } from 'apprise-frontend-core/intl/language'
import { useForm } from 'apprise-frontend-core/utils/form'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { Drawer, DrawerProps } from 'apprise-ui/drawer/drawer'
import { FieldInfo } from 'apprise-ui/field/model'
import { useValidation } from 'apprise-ui/field/validation'
import { Form } from 'apprise-ui/form/form'
import { RouteGuard } from 'apprise-ui/link/routeguard'
import { TextBox } from 'apprise-ui/textbox/textbox'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { SaveIcon } from 'apprise-ui/utils/icons'
import { useContext, useEffect } from 'react'
import { SearchIcon } from './const.ui'
import { SavedQueryContext } from './context'
import { Query, SavedQuery, isSavedQuery } from './model'
import { useSearchPreferences } from './utils'


// return functions to control the drawer of a <SavedQueryEditor>.
// takes a query on open, either a previosly SavedQuery or a Query to save for the first time.
// uses a dedicated <SavedQueryContext> to exchange the query with the editor, so clients can be anywhere below it. 
// a default context is mounted by <SearchPage>.

export const useSavedQuery = () => {

   // const t = useT()

    const context = useContext(SavedQueryContext)

    
    const openWith = (query: Query | SavedQuery) => {

        context.set(s => s.query = isSavedQuery(query) ? query : { name: undefined!, query })

    }

    const close = () => context.set(s => s.query = undefined)
    
    const currentQuery = context.get().query

    return { currentQuery, openWith, close  }

}


export const SavePanel = (props: DrawerProps) => {

    const t = useT()

    const { className, ...rest } = props

    const { close, currentQuery } = useSavedQuery()

    const preferences = useSearchPreferences()

    const { edited, set, dirty, reset, initial } = useForm(currentQuery)

    useEffect(()=> {

        if (currentQuery){
            reset.to(currentQuery).quietly()
        }

    // eslint-disable-next-line
    }, [currentQuery])

    const { queries=[] } = preferences.get()

    const otherQueries = queries?.filter(q => q.name !== initial?.name)

    const { check, is } = useValidation()

    const nameInfo: FieldInfo = {

        label: t('search.save_query_name_lbl'),
        msg: t('search.save_query_name_msg'),

        // if we have a 
        ...check(is.duplicateWith(otherQueries, q => q.name)).provided(!!edited).on(edited)

    }

    const disabled = !edited?.name || nameInfo.status === 'error'

    const saveQuery = () => {

        preferences.set(prefs => {

            const current = prefs?.queries

            if (!current?.length)

                prefs.queries = [edited!]


            else

                prefs.queries = initial?.name ? prefs.queries.map(q => q.name === initial!.name ? { ...q, name: edited!.name } : q) : [edited!, ...prefs.queries!]


        })


        reset.to(edited).quietly()

        close()

    }


    const saveBtn = <Button type='primary' icon={<SaveIcon />} enabled={dirty} disabled={disabled} onClick={saveQuery}>
        {t("search.save_query_btn")}
    </Button>

    return <Drawer open={!!currentQuery} onClose={close} className={classname('save-query', className)} width={450} icon={<SearchIcon />} title={t('search.save_query_title')} {...rest}>

        <ExplainerPanel>

            <Explainer icon={<SearchIcon />} iconFill action={saveBtn}>
                {t('search.save_query_explainer')}
            </Explainer>

            <Form style={{ marginTop: 40 }}>

                <TextBox enabled={!!edited} info={nameInfo} placeholder={t('search.save_query_name_placeholder')} onChange={set.with((m, n) => m!.name = n)}>
                    {edited?.name}
                </TextBox>

            </Form>

            <RouteGuard when={dirty} />

        </ExplainerPanel>


    </Drawer>
}