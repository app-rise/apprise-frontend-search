import { useT } from 'apprise-frontend-core/intl/language'
import { useBusyGuardIndicator } from 'apprise-frontend-core/utils/busyguard'
import { utils } from 'apprise-frontend-core/utils/common'
import { useSettings } from 'apprise-frontend-streams/settings/api'
import { Styled, classname } from 'apprise-ui/component/model'
import { Pagination } from 'apprise-ui/pagination/pagination'
import { TableContext } from 'apprise-ui/table/context'
import { Table, TableProps } from 'apprise-ui/table/table'
import React, { PropsWithChildren, useContext, useEffect } from 'react'
import { useSearchContext } from './boundary'
import { useSearchProfile } from './config'
import { defaultPageSize, searchType } from './const'
import { useCurrentQuery } from './context'
import { Result } from './model'
import './resulttable.scss'
import { useSearchEffects } from './searcheffects'
import { SearchSettings } from './settings'

export type ResultTableProps = PropsWithChildren<Styled & {


}>

export const TableConfig = <R extends Result>(_: TableProps<R>) => null

export const searchTableName = 'search-results'

export const ResultTable = (props: ResultTableProps) => {

    const t = useT()

    const { className, style, ...rest } = props

    const { dirty } = useCurrentQuery()

    const state = useSearchContext()

    const effects = useSearchEffects()

    const Indicator = useBusyGuardIndicator()

    const { table } = useSearchProfile().props('table',TableConfig).get()

    const { pageSize = defaultPageSize } = useSettings<SearchSettings>(searchType)


    const { children: tablechildren, ...tableprops } = table

    const { results, page, total } = state.get()

    useSortChangeMonitor()

    return <div className={classname('result-table', className, dirty && 'search-dirty')} style={style} {...rest}>
        <Indicator>

            <Table name={searchTableName} noFilter data={results} total={total} {...tableprops}>

                <Table.Counter mode='total' overflow={999999} >
                    <span className='counter-label'>{t('search.counter_label')}</span>
                </Table.Counter>


                <Table.Control>
                    <Pagination total={total} pageSize={pageSize} onChange={effects.changePage}>
                        {page}
                    </Pagination>
                </Table.Control>

                {utils().elementsIn(tablechildren).map((e,i) => React.cloneElement(e, {key:i}) )}

            </Table>

        </Indicator>
    </div>
}

const useSortChangeMonitor  = () => {

    const { sort: sortspecs } = useContext(TableContext).get()[searchTableName] ?? {}

    const effects = useSearchEffects()

    const query = useCurrentQuery()

    const state = useSearchContext()

    const changeSortAndResetQuery = async () => {

        if (sortspecs) {
        
            await effects.changeSort(sortspecs)
        
            query.reset.to(state.get().query).quietly()
            
        }
    }

    useEffect(() => {

        changeSortAndResetQuery()

        // eslint-disable-next-line
    }, [sortspecs])

}