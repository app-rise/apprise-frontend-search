import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { useStable } from 'apprise-frontend-core/utils/function'
import { Button, ButtonProps } from 'apprise-ui/button/button'
import { Tip } from 'apprise-ui/tooltip/tip'
import { RemoveItemIcon } from 'apprise-ui/utils/icons'
import { FC, PropsWithChildren, useEffect, useState } from 'react'
import { AiFillPlusCircle } from 'react-icons/ai'
import { useSearchContext } from './boundary'
import { useSearchProfile } from './config'
import { useCurrentQuery } from './context'
import { Query, SearchCondition, SearchConditionData } from './model'
import { QueryFieldProps, TokenizedField } from './queryfield'
import { classname } from 'apprise-ui/component/model'


// a pill can be selected to collect the values required to form a condition over some property of the target model.
// a repeatable pill can be selected multiple times to form multiple conditions over the same property.

export type QueryPillProps<C extends SearchConditionData = SearchConditionData> = {

    condition: string        // separates unrelated pills (different field, different conditions).
    id: string          // separates related pills (same field, multiple conditions).


    label: string       // short name of the pill (serves as field label).
    title: string       // long nam eof the pill (used in tooltips). 

    field: FC<QueryFieldProps<C>>   // the input field that gather values for the condition.

    tab?: string
    modes?: []                  // the modes required for the piull to be available for selection.

    repeatable?: boolean        // if the pill can be selected multiple times to form multiple conditions over the same field. 
    defaultLayout?: boolean     // if one instancee of the pill should be preselected in the query panel.

}

export const QueryPill = <C extends SearchConditionData = SearchConditionData>(_: Partial<QueryPillProps<C>>) => null


const defaultTab = 'default'

export const useQueryPills = (props: PropsWithChildren<Partial<{

    filterPills: (_: QueryPillProps) => boolean

}>>) => {

    const state = useSearchContext()

    const { initial: initialQuery, edited: editedQuery, set: setQuery } = useCurrentQuery()


    const { pills } = useSearchProfile().all('pills', QueryPill).get()

    const unfilteredpillprops = pills

        .map((c, i) => ({       // defaults for prototoyping

            condition: `Pill ${i}`,
            label: `Pill ${i}`,
            field: TokenizedField,
            tab: defaultTab,

            ...c.props

        }) as QueryPillProps)


    const tabs = utils().dedup(unfilteredpillprops.filter(p => p.tab).map(p => p.tab as string))

    const [selectedTab, setSelectedTab] = useState(tabs[0])

    // adds a pill for each condition already in the query,
    // forces at least one for pills configured for the default layout.
    const initialLayout = (query: Query): QueryPillProps[] => unfilteredpillprops

        .filter(p => p.defaultLayout || query.conditions[p.condition])
        .flatMap(p => (query.conditions[p.condition] ?? [{ id: p.id } as SearchCondition]).map(c => ({ ...p, id: c.id ?? utils().mint() })))


    const defaultPillFilter = (p: QueryPillProps) => (!p.modes || p.modes.some(editedQuery.modes.includes)) && (!selectedTab || p.tab === selectedTab)


    const { filterPills = defaultPillFilter } = props

    // applies filters, eg. basd on mode.
    const pillprops = unfilteredpillprops.filter(filterPills)




    const [selected, setSelected] = useState(initialLayout(editedQuery))

    //console.log({conditions: editedQuery.conditions})

    const lastStored = state.get().query

   // reacts to external query changes outside to resync the editor.
    // doesn't resync with a reset though, simply dirties it to show any result is stale.
    useEffect(() => {

        if (lastStored && lastStored !== initialQuery) {

            setQuery.to(lastStored)
            setSelected(initialLayout(lastStored))
        }

        // eslint-disable-next-line
    }, [lastStored])



    const removeCondition = (pill: QueryPillProps) => setQuery.using(query => {

        query.conditions[pill.condition] = query.conditions[pill.condition].filter(c => c.id !== pill.id)

        // removes pill entirely for an accurate dirty check.
        if (query.conditions[pill.condition].length === 0)
            delete query.conditions[pill.condition]
    })


    const updateCondition = (pill: QueryPillProps, condition: SearchCondition) => setQuery.using(query => {

        //console.log("updating", { pill, condition, current: query.conditions[pill.name] })

        // no previous instance?
        if (!query.conditions[pill.condition]) {

            query.conditions[pill.condition] = [condition]

            return
        }

        // updates an existing instance?
        const index = query.conditions[pill.condition]?.findIndex(c => c.id === pill.id)

        // doesn't, append.
        if (index < 0)
            query.conditions[pill.condition].push(condition)

        // does, replace.
        else
            query.conditions[pill.condition][index] = condition

    })

    const selectTab = (tab: string) => setSelectedTab(tab)

    const change = (props: QueryPillProps) => (condition?: SearchCondition) => {

        // 
        if (condition && Object.values(condition.data).flat().length)
            updateCondition(props, condition)
        else
            removeCondition(props)

    }

    const select = (pill: QueryPillProps) => setSelected([...selected, { ...pill, id: utils().mint() }])


    const deselect = (pill: QueryPillProps) => {

        setSelected(s => s.filter(p => p.id !== pill.id))

        // pill may have been used already, remove corresponding condition from query.
        removeCondition(pill)
    }



    const SelectedPill = useStable((props: SelectedQueryPillProps) => {

        const defaultDisabled = selected.length === 1
        const defaultChange = change(props)
        const defaultDeselect = () => deselect(props)
        const defaultCondition = editedQuery.conditions[props.condition]?.find(ps => ps.id == props.id)

        //console.log({defaultCondition})

        const { disabled = defaultDisabled, value = defaultCondition, onChange = defaultChange, onDeselect = defaultDeselect } = props

        return <SelectedQueryPill disabled={disabled} value={value} onChange={onChange} onDeselect={onDeselect} {...props} />

    })


    const PillButton = useStable((props: QueryPillButtonProps) => {

        const defaultSelect = () => select(props)
        const defaultDisabled = () => !props.repeatable && selected.some(p => p.condition === props.condition)

        const { onSelect = defaultSelect, disabled = defaultDisabled() } = props

        return <QueryPillButton disabled={disabled} onSelect={onSelect} {...props} />

    })

    const TabButton = useStable((props: QueryTabProps) => {

        const defaultSelected = selectedTab === props.name

        const defaultSelect = () => selectTab(props.name)

        const { onSelect = defaultSelect, selected = defaultSelected, ...rest } = props


        return <QueryTabButton selected={selected} onSelect={onSelect} {...rest} />

    })


    return { all: pillprops, selected, tabs, SelectedPill, PillButton, TabButton, select, deselect, change }

}



type SelectedQueryPillProps = QueryPillProps & {

    disabled?: boolean
    value?: SearchCondition | undefined,
    onChange?: (_: SearchCondition | undefined) => void
    onDeselect?: () => void
}

export const SelectedQueryPill = (props: SelectedQueryPillProps) => {

    const t = useT()

    const { condition, label, title, field: QueryField, onDeselect, onChange = () => { }, value, disabled } = props

    const tippedLabel = label ? <Tip tip={t(title)}>{t(label).toLowerCase()}</Tip> : undefined

    const updateCondition = (data: SearchConditionData | undefined) => onChange(data ? { id: props.id, data } : data)

    return <div className='query-pill-selected' style={{ display: 'flex' }} >
        <QueryField condition={condition} label={tippedLabel} onChange={updateCondition}>
            {value?.data}
        </QueryField>
        <Button disabled={disabled} noLabel className='query-pill-selected-close' type='ghost' icon={<RemoveItemIcon size={12} />} onClick={onDeselect} />
    </div >

}

export type QueryPillButtonProps = QueryPillProps & {

    onSelect?: () => void
    disabled?: boolean


}

export const QueryPillButton = (props: QueryPillButtonProps) => {

    const t = useT()

    const { label, title, onSelect, disabled } = props

    return <Button tip={t('search.pill_unselected_tip', { title: t(title) })} tipDelay={0.8} tipPlacement='top' disabled={disabled} iconPlacement='left' icon={<AiFillPlusCircle />} type='ghost' className='query-pill-btn' size='small' onClick={onSelect}>
        {t(label).toLowerCase()}
    </Button>
}


export type QueryTabProps = ButtonProps & {

    name: string

    selected?: boolean
    onSelect?: () => void

}

export const QueryTabButton = (props: QueryTabProps) => {

    const t = useT()

    const { name, selected, onSelect, ...rest } = props

    const tip = t(`search.pill_tab_${name}_tip`, 'none')


    const classes = classname('query-pill-tab-btn', 'query-pill-btn', selected && 'pill-tab-selected')

    return <Button tip={tip === 'none' ? undefined : tip} tipDelay={0.8} tipPlacement='top' type='ghost' className={classes} size='small'
        onClick={onSelect} {...rest}>
        {t(`search.pill_tab_${name}`, name)}
    </Button>
}

