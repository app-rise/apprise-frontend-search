import { ComponentProps, PropsWithChildren } from 'react';

import { NoSuchRoute } from 'apprise-ui/link/nosuchroute';
import { Route, Switch } from 'react-router';
import { useSearchProfile } from './config';


export const searchRoute = "/search"


export const SearchRoute = (_: ComponentProps<typeof Route>) => null

//SearchRoute[elementProxyRole] = true



export const SearchRouter = (props: PropsWithChildren) => {

    const { children } = props

    const { routes } = useSearchProfile().all('routes', SearchRoute).get()

    return <Switch>

        {routes.map((r,i) => <Route key={i} {...r.props} />)}

        <Route exact path={searchRoute}>
            {children}
        </Route>

        <NoSuchRoute />

    </Switch>
}