import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { Button } from 'apprise-ui/button/button'
import { Label, LabelProps } from 'apprise-ui/label/label'
import { SidebarContent } from 'apprise-ui/page/sidebar'
import { Table } from 'apprise-ui/table/table'
import { useTableUtils } from 'apprise-ui/table/utils'
import { DateIcon, SaveIconFull } from 'apprise-ui/utils/icons'
import { FC, Fragment, useCallback, useMemo } from 'react'
import { useSearchContext } from './boundary'
import { useCurrentQuery } from './context'
import { DefaultHistoryDateLabel } from './historydatelabel'
import { HistoryItem, Query } from './model'
import { useSavedQuery } from './savequery'
import "./searchhistory.scss"
import { useSearchPreferences } from './utils'

type HistoryRow = HistoryItem & {

    name: string | undefined

    disabled: boolean,
    highlighted: boolean
}



export type HistoryyDateLabelProps = Partial<LabelProps & {

    dateFormat: string
    dateMode: 'short' | 'long'
    dateLabel: FC<HistoryyDateLabelProps>

}>



export type HistoryProps = HistoryyDateLabelProps & {

    dateProps: HistoryyDateLabelProps
    dateLabel: FC<HistoryyDateLabelProps>

}


export const SearchHistory = (props: Partial<HistoryProps>) => {

    const t = useT()

    const state = useSearchContext()

    const { edited, dirty } = useCurrentQuery()

    const drawer = useSavedQuery()

    const { dateProps, dateLabel: HistoryDate = DefaultHistoryDateLabel } = props

    const { history } = state.get()

    const preferences = useSearchPreferences()

    const { queries : savedqueries = []} = preferences.get()

    const nameOf = useCallback((query: Query) => savedqueries.find(saved => utils().deepequals(saved.query, query))?.name, [savedqueries])


    // decorates history with UI state.
    const historyRows: HistoryRow[] = useMemo(() =>

        history.map(item => ({

            disabled: dirty
            ,

            name: nameOf(item.query)

            ,

            highlighted: utils().deepequals(edited, item.query)

            ,

            ...item
        }))


        , [dirty, nameOf, history, edited])

    const { Column } = useTableUtils<HistoryRow>()

    const select = (query: Query) => () => state.set(s => s.query = query)
    const save = (query: Query) => () => drawer.openWith(query)

    return <div className='search-history'>

        <SidebarContent.Title>{t('search.history_title')}</SidebarContent.Title>

        <Table.Sider<HistoryItem> name='queryhistory' noFilter emptyPlaceholder={t('search.history_placeholder')} rowId={i => i.timestamp} data={historyRows} mountDelay={0} >

            <Column render={row => {

                const { name, query, timestamp, highlighted, disabled } = row

                const content = name ?

                    <Label tip={t('search.history_load_tip')} disabled={disabled} icon={<DateIcon />} title={name} highlighted={highlighted} />

                    :

                    <HistoryDate noReadonly tip={t('search.history_load_tip')} date={timestamp} highlighted={highlighted} {...dateProps} />

                return highlighted ? content : <Button readonly={highlighted} onClick={select(query)}>{content}</Button>

            }} />



            <Column align='right' width={40} render={({ query, highlighted, total, disabled }) =>

                <Fragment>
                    <Label className='history-count' tip={t('search.history')} noIcon highlighted={highlighted} title={total} />
                    <Button noLabel icon={<SaveIconFull color={disabled ? 'inherit' : 'lightseagreen'} />}
                        className='history-save' tip={t('search.history_save_tip')}
                        disabled={disabled} onClick={save(query)} />

                </Fragment>}

            />

        </Table.Sider>

    </div>
}

