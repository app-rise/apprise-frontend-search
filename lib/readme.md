# Search As a Lib


We provide application support for ****structured search****, where:

- users assemble ad-hoc ****queries**** over some target data type.
- queries are logical combinations of simple conditions defined over key properties of the target type. 
- queries run over the network and produce ****results**** paginated for browsing. 
- results can be sorted by one or more properties.

We then add support for:

- keeping the history of queries that run in the current session.
- saving queries across sessions as user preferences.
- downloading result subsets in Excel format.
  
## Default Layout

Search is a pretty high-level functionality, it encompasses logic, state, and layout.

For layout, we offer a ready-made solution where:

- search gets a dedicated `<SearchSection>`.
- the section renders a single `<SearchPage>`.
- the page contains a `<SearchPanel>`, but also mounts a `<SavePanel>` and a `<DownloadPanel>` that render drawers on demand. 
- the page sidebar renders the `<SearchHistory>`,  the list of `<SavedSearches>`, and a button to download results. 
- the search panel renders a `<QueryPanel>` over a `<ResultTable>`.
- the query panel lists `<QueryPill>`s for each searchable conditions, and each selected pill renders a `<QueryField>` to gather values for its condition.
- the query panel also shows a set of pre-defined `<QueryAction>`s to run, reset, revert, and save queries. 
- the table maps results to rows and properties to columns, and it renders controls for pagination and sorting. 

## Contexts and Boundaries

`<Search>` keeps the search state in context, eg. the  query, its results, the current page, etc.

In the common use case:

- applications search over a single data type.
- they mount  `<Search>` high up in the component tree, to preserve search state as users navigate back to the section.

In the advanced use case:

- applications search over multiple data types.
- so they need to maintain state in separate contexts.

These applications now can't mount multiple `<Search>` at the top, as the innermost component would mask the others, eg:

``` html
<Search>                 // apple context
  <Search>               // orange context, hides apples.
    ...
      <SearchSection>     // apple section/page
        ...
      <SearchSection>     // orange section/page
        ...
```

To address this we let applications:

- create **different** contexts to hold separate pieces of state. So there's no masking at the top.
- store contexts themselves at the level of `<SearchSection>`s, so components below can discover which one to lookup.

We wrap the approach around a "boundary" concept:

- ****`createSearchBoundary()`**** mints a dedicated context in module scope.
- ****`<SearchBoundary>`**** stores a context itself, it's a "meta-context".
- ****`useSearchContext()`**** lookups up the boundary to find the context, then looks up the context to get to the right piece of search state up at the top.

```js 
const apples = createSearchBoundary()      
const oranges = createSearchBoundary()
```

```html
<Search boundary={apples}>
  <Search boundary={oranges}>         // no mixing apples with oranges.
     ...
   <SearchBoundary boundary={apples}>     // apples in scope below.
        <SearchSection>                  
          ...                             // useSearchBoundary() => apples

   <SearchBoundary boundary={oranges}>    // oranges in scope below.
        <SearchSection>                                   
          ...                             // useSearchBoundary() => oranges           
```


To simplify usage further, we let `<SearchSection>` create boundaries on behalf of the app, eg:

```html
<Search boundary={apples}>
  <Search boundary={oranges}>         // no mixing apples with oranges.
     ...
   <SearchSection boundary={apples}>     // apples in scope below.
        ...                            // useSearchBoundary() => apples

   <SearchSection boundary={oranges}>    // oranges in scope below.
        ...                            // useSearchBoundary() => oranges           
```

Applications can also specify a `boundary` on a  `<SearchPage>` if:

- don't use the default `<SearchSection>`.
- use multiple pages below a `<SearchSection>` .


## Custom Layout

The default layout allows for application-specific *configuration*, and often requires it. 

> for example, applications must define `<QueryPill>`s for the default `<QueryPanel>` , `<Column>`s for the `<ResultTable>`, and maybe some extra entries in the sidebar of the `<SearchPage>`.

Where configuration isn't enough, applications may need to *replace* default components with custom ones. 

> for example, an application may be forced to use a custom query panel or result table, or they may want to swap out the `<SearchPanel>` for one where the table bleeds to fill `<SearchPage>` and the `<QueryPanel>` is in a drawer. 

And yet some applications may require other forms customisations, like repeating default components, or interposing custom components in the default hierarchy.

> for example, an application may require multiple pages under a `<SearchSection>` - perhaps, but  not necessarily, two `<SearchPage>`s - hence a router in between section and pages.

This level of customisation is hard to afford, even the simplified forms that align with real use cases.   

Our first problem is how to let applications configure components deep down the layout without asking them re-assemble enough of the layout just to reach them. 

> for example, how can they configure `QueryPill`s without re-assembling the layout of section, pages, and panels required to react the `<QueryPanel>`?

To solve this problem,  we first introduce `<SearchProfile>` as a container of *configuration carriers*. These are components that render nothing but can be analysed at render-time to find configuration in their properties.  

For example,  `<SidebarConfig>` carries some configuration for `<SearchPage>` and `<QueryPill>`s  carry it for the `<QueryPanel>`:

```html
<SearchProfile>
   ...
	<SidebarConfig defaultOpen />
	...
	<QueryPill name={...} field={...}/>
	<QueryPill name={...} field={...}/>
	<QueryPill name={...} field={...}/>
    ...
</SearchProfile>
```

Then we let applications mount the profile high up in the tree, as long as there is a `<ConfigurationProfileProvider>` above, where the profile can store its child wrappers. 

Now any component below the provider can lookup the profile and extract the wrappers that contain their configuration. 

We define `useSearchProfile()` as a utility for that, allowing components to extract wrappers or their properties, as singleton or arrays. For example:

```js
 const { panel, page, actions } = useSearchProfile()
 .props('panel', QueryPanelConfig)
 .one('page', PageConfig)
 .all('actions', QueryAction)
 .get()
```


Finally,  we mount automatically `<ConfigurationProfileProvider>` in  `<SearchSection>` and `<SearchPage>` so applications don’t have to.

In the common case, applications can mount a profile inside `<SearchSection>` and have it reach default components deep down the section:

```html
<SearchSection ...>
  <SearchProfile>
	...wrappers...
  </SearchProfile>
</SearchSection>
```

We can use the `<SearchProfile>` for component replacements too, because a custom implementation is a form of configuration:

```html
<SearchProfile>
	...
	<QueryPanelAlternative component={CustomPanel} />
	...
</SearchProfile>
```


Here `<QueryPanelAlternative>` serves as a configuration for the `<SearchProfile>`, which can then render it in place of the  default`<QueryPanel>`.

At the time of writing, in fact, the example above illustrates a pattern but we don't support it ue. We don’t pre-define any alternative elements, as the possibilities upfront are far too many. We plan to do it on demand, i.e. driven by specific use cases.

We support one last form of customisation, where applications can pass custom children to  `<SearchSection>` and `<SearchPage>`, as a signal that they should be rendered in place of, respectively, `<SearchPage>` and `<SearchPanel>`. 

For example, `<SearchSection>` renders the `<CustomRouter>` in place of the `<SearchPage>`. The router may then render the `<SearchPage>`, or something else altogether. :

```html
<SearchSection>
	...
	<CustomRouter />
</SearchSection>
```

Finally, applications that replace default components may still want to use them selectively. For example, an application that swaps out the default `<SearchPanel>` may still want to use the actions and their logic.

We support this by assembling default components out of smaller components, so that they can be cherry-picked. This approach is enabled by using context heavily for state sharing, keeping the use of props to a minimum.