
import React from 'react';
import { ResultCacheContext } from './context';




export const useResultCache = <T = any>(props : Partial<{

    idOf: (_: T | undefined ) => string | undefined

}>={}) => {

    const { idOf = r => r?.['id'] } = props

    const cache = React.useContext(ResultCacheContext)

    const self = {


        get: (id?: string) => id ? cache.get()[id] as T : undefined

        ,

        set: (result?: T) => cache.set(c => c[idOf(result)] = result)

        ,


        reset: (ids: string[] = []) => ids.forEach(id => cache.set(c => delete c[id]))


    }

    return self


}