import { useToggles } from 'apprise-frontend-core/toggle/toggles'



export const allSearchToggles = ["search"] as const

export type SearchToggle = typeof allSearchToggles[number]

export const useSearchToggles = () => {

    const toggles =  useToggles<SearchToggle>()

    const self = {

        get search() { 
            return toggles.isActive('search')
        }
    }


    return self

}