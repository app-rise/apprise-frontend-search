import { State } from 'apprise-frontend-core/state/api'
import { Fragment, PropsWithChildren, useContext } from 'react'
import { SearchBoundaryContext, SearchContextType, SearchState } from './context'
import { Query, Result } from './model'




export const useSearchContext = <Q extends Query = Query, R extends Result = Result>() => useContext(useContext(SearchBoundaryContext)) as State<SearchState<Q, R>>


export type SearchBoundaryProps = PropsWithChildren<{

    boundary: SearchContextType

}>


// if provided, puts a search context in scope, so that children may look it up and separate it from other such contexts.
// puts also a content for configuration in scope, it there isn't one already, so that a <SearchProfile> below may fill it. 
export const SearchBoundary = (props: Partial<SearchBoundaryProps>) => {

    const { children, boundary } = props

    if (boundary)
        return <SearchBoundaryContext.Provider value={boundary}>
            {children}
        </SearchBoundaryContext.Provider>

    return <Fragment>
        {children}
    </Fragment>

}
