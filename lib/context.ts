import { fallbackStateOver, State } from 'apprise-frontend-core/state/api'
import { FormState } from 'apprise-frontend-core/utils/form'
import { Context, createContext, useContext } from 'react'
import { defaultStartQuery } from './const'
import { HistoryItem, Query, Result, SavedQuery } from './model'
import { useDefaultSearchPreferences } from './preferences'
import { SearchConfigurationProfile } from './config'
import { SearchPreferenceHandler } from './preferences'



// the state that powers a search page.

export type SearchState<Q extends Query = Query, R extends Result = Result> = {

    startQuery: Q
    query: Q
    results: R[]
    history: HistoryItem[]
    page: number
    total: number
    usePreferenceHandler: () => SearchPreferenceHandler
}


export const initialSearchState: SearchState = {

    startQuery: defaultStartQuery,
    results: undefined!,        // no query executed
    query: undefined!,          // no query
    history: [],
    page: 1,
    total: undefined!,
    usePreferenceHandler: useDefaultSearchPreferences
}


// note: readme.md tells the story below.

// the type of all search contexts.
export type SearchContextType = Context<State<SearchState>>

// minsts a new search context.
export const createSearchBoundary = () => createContext<State<SearchState>>(fallbackStateOver(initialSearchState))

// the default search context.
export const DefaultSearchContext: SearchContextType = createSearchBoundary()

// holds the search context in scope.
export const SearchBoundaryContext = createContext<SearchContextType>(DefaultSearchContext)



export type QueryState<Q extends Query = Query> = FormState<Q>

export const QueryContext = createContext<QueryState>(undefined!)


export const useCurrentQuery = <Q extends Query = Query>() => useContext(QueryContext) as unknown as QueryState<Q> ?? {}

export type SavedQueryState = {

    query: SavedQuery | undefined
}

export const initialSavedQueryState: SavedQueryState = {

    query: undefined
}

export const SavedQueryContext = createContext<State<SavedQueryState>>(fallbackStateOver(initialSavedQueryState))


// holds and shares the application's configuration profie.
export const SearchProfileContext = createContext<State<{profile: SearchConfigurationProfile}>>(fallbackStateOver({profile: () =>[] as any})) 


export type ResultCache<T=any> = Record<string, T>

export const ResultCacheContext = createContext<State<ResultCache>>(fallbackStateOver({}))