import { Language } from 'apprise-frontend-core/intl/language'
import { SortSpec } from 'apprise-ui/table/sorting'


export type Query = {

    conditions: Record<ConditionType,SearchCondition[]>         // one more more conditions grouped by type.
    sort: SortSpec[]
    modes: string[]
}

export type ConditionType=string            // separates related conditions from others.

export type SearchCondition = {             // an instance of a condition of a given type
    
    id: string,                             // identifies the instance.
    data: SearchConditionData               // instance data.

}

export type SearchConditionData = Record<string, any>       // conditions can have arbitrary shapes.



export type SavedQuery<Q extends Query=Query> = {
    name: string
    query: Q 
}

export const isSavedQuery = <Q extends Query=Query> (query : Q | SavedQuery<Q>) : query is SavedQuery<Q>  => !!(query as any)?.query

export type Sort = { field: string, mode: 'asc' | 'desc' }


export type SearchRequest <Q extends Query=Query> = {

    query: Q
    language: Language

    total?: number
    cursor: SearchCursor

    source?: 'system' | 'user'
    silent?: boolean

}

export type SearchMode = string


export type PageCursor = Partial<{

    page: number
    pageSize: number


}>

export type RecordCursor = Partial<{

    offset: number
    limit: number

}>


export type SearchCursor = PageCursor | RecordCursor



export type HistoryItem = {
   
    timestamp: number
    source: SearchRequest['source']
    query: SearchRequest['query']
    total: SearchRequest['total']
}


export type Result = Record<string, any>

export type ResultBatch<R extends Result = Result> = {

    results: R[]
    total: number

}
