import { getServer } from 'apprise-backend-helper/api/server'
import { searchApi, SearchDto } from './api.js'
import { getDao } from './dao.js'
import { ResultBatch } from './model.js'
import { getSearchConfig } from './init.svc.js'



export const initSearchApi = () => {

    const server = getServer()

    const config = getSearchConfig()

    const dao = getDao()

    server.post<{ Body: SearchDto, Reply: ResultBatch }>(searchApi, request => {

        return dao.searchPage(config.requestHandler(request.body))

    })


}