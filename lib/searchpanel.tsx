import { Styled, classname } from 'apprise-ui/component/model'
import { PropsWithChildren } from 'react'
import { QueryPanel } from './querypanel'
import { ResultTable } from './resulttable'
import "./searchpanel.scss"

export type SearchPanelProps = PropsWithChildren<Styled & {

}>

// renders a default layout for search, defaulting to a <QueryPanel> directly above a <ResultTable>.
export const SearchPanel = (props: Partial<SearchPanelProps>) => {

    const { children, className, style, ...rest } = props

    return <div className={classname('search-panel', className)} style={style} {...rest}>

        {children}

        <QueryPanel />
        <ResultTable />

    </div>
}