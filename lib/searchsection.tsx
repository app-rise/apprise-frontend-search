
import { useT } from 'apprise-frontend-core/intl/language'
import { elementProxyRole, utils } from 'apprise-frontend-core/utils/common'
import { SectionModel } from 'apprise-ui/page/model'
import { Section } from 'apprise-ui/scaffold/scaffold'
import { SearchBoundary, SearchBoundaryProps } from './boundary'
import { ConfigurationProfileProvider, ConfigurationProfileProviderProps } from './config'
import { searchRoute } from './router'
import { SearchIcon } from './const.ui'
import { SearchRouter } from './router'
import { SearchPage } from './searchpage'

// renders a section for search with custom children or a default <SearchPage>.
// wraps around them:
// 1 a <SearchConfigurationProvider>, to shate witb all children the config in a <SearchProfile> below. 
// 2. an optional <SearchBoundary>, to connect with a specific search context.

export function SearchSection(props: Partial<SectionModel & SearchBoundaryProps & Partial<ConfigurationProfileProviderProps>> = {}) {

    const t = useT()

    const { children, profile, ...rest } = props

    const hasChildren = utils().elementsIn(props.children).length

    return <Section icon={<SearchIcon />} title={t('search.singular')} route={searchRoute} {...rest}>

        <SearchBoundary {...props}>

            <ConfigurationProfileProvider profile={profile}>

                <SearchRouter>

                    {children}

                    {   //returns a standard page, unless the application takes over with specific children.
                        hasChildren || <SearchPage />
                    }

                </SearchRouter>

            </ConfigurationProfileProvider>

        </SearchBoundary>

    </Section>


}

SearchSection[elementProxyRole] = true