import { StateProvider } from 'apprise-frontend-core/state/provider';
import { utils } from 'apprise-frontend-core/utils/common';
import { FC, PropsWithChildren, useContext } from 'react';
import { SearchProfileContext } from './context';
import { useMode } from 'apprise-frontend-core/config/api';


// the type of functions that return configuration carriers with application customisations.
export type SearchConfigurationProfile = () => JSX.Element | JSX.Element[]


// processes configuration wrappers in the current search profile, if any exists.
// extracting components or their properties.
export const useSearchProfile = () => {

    const { profile } = useContext(SearchProfileContext).get() ?? {}

    // instantiates the profile to get to the configuration carriers )(simulates render).
    const instance = profile?.()

    // extracts carriers, top-level or in side arrays/proxies/fragments.
    const config = utils().elementsIn(instance ?? [])

    const funrec = <T extends object>(value: T) => {

        return {

            one: <S extends string, T extends any>(name: S, component: FC<T>) => {

                const ext = { [name]: config.find(utils().isElementOf(component)) } as Record<S, JSX.Element | undefined>

                return funrec({ ...value, ...ext })
            }

            ,


            all: <S extends string, T extends any>(name: S, component: FC<T>) => {

                const ext = { [name]: config.filter(utils().isElementOf(component)) } as Record<S, JSX.Element[]>

                return funrec({ ...value, ...ext })
            }

            ,

            props: <S extends string, T extends any>(name: S, component: FC<T>) => {

                const ext = { [name]: config.find(utils().isElementOf(component))?.props ?? {} } as Record<S, T>

                return funrec({ ...value, ...ext })
            }

            ,

            propsOfAll: <S extends string, T extends any>(name: S, component: FC<T>) => {

                const ext = { [name]: config.filter(utils().isElementOf(component)).map(e => e.props) } as Record<S, T[]>

                return funrec({ ...value, ...ext })
            }

            ,

            get: () => value


        }

    }

    return funrec({})

}


export type ConfigurationProfileProviderProps = PropsWithChildren<{

    profile?: SearchConfigurationProfile
}>

// shares configuration carriers with children if it needs and if it finds a profile that holds them. 
export const ConfigurationProfileProvider = (props: ConfigurationProfileProviderProps) => {

    const { profile, children } = props

    const parent = useContext(SearchProfileContext).get()

    const { development } = useMode()

    // do nothing if we're already sharing up in the hierarchy.
    if (!parent || !profile)
        return children

    return <StateProvider initialState={{ profile }} context={SearchProfileContext}>
        {development && 

            /* mounts the profile (hiding it) so that it may trigger on HMR and change the context. */
            <ProfileListener Profile={profile} />
        
        }
        {children}
    </StateProvider>

}



const ProfileListener = (props: { Profile: SearchConfigurationProfile }) => {

    const { Profile } = props

    return <div style={{ display: 'none' }}>
        <Profile />
    </div>

}