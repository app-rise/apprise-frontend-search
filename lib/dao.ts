import { getSql } from 'apprise-backend-helper/db/sql'
import { dbutils } from 'apprise-backend-helper/db/utils'
import { PageCursor, RecordCursor, ResultBatch, SearchCursor } from 'apprise-frontend-search/model'
import { defaultPageSize } from 'apprise-frontend-search/const'
import { getSearchConfig } from './init.svc'
import { SearchDto } from './api'




export const isPageCursor = (cursor: SearchCursor) : cursor is PageCursor => !!(cursor as PageCursor).page || !!(cursor as PageCursor).pageSize 


const normalise = (cursor: SearchCursor) => {

    const limit = (isPageCursor(cursor) ? cursor.pageSize : cursor.limit) ?? defaultPageSize
    const offset = (isPageCursor(cursor) ? Math.max(0, ((cursor.page ?? 1) - 1) * limit) : cursor.offset ?? 0) ?? defaultPageSize

    return { offset, limit } satisfies RecordCursor
}

export type TypedDirective = { type: string }

const isTypedDirective = <T>(condition: T): condition is TypedDirective & T => !!(condition as TypedDirective).type

export const getDao = () => {

    const { table, jsoncol, conditionHandlers, sortHandlers } = getSearchConfig()

    const sql = getSql()

    const { chain } = dbutils()


    const self = {


        searchPage: async (dto: SearchDto) => {

            const { conditions, sort } = dto

            const typedConditions = conditions.filter(isTypedDirective) 

            
            // main query: where clause.
            const whereClause = chain( typedConditions.map(c => conditionHandlers[c.type]?.(c).whereFragment))
                .with(sql` and `)
                .forClause(sql`where`)
                .orFallbackTo(sql`true`)
                .done()

          
            const orderByClause = chain(
                
                sort.map(s => {

                    const mode = sql`${(s.mode ?? 'asc') === 'asc' ? sql`asc` : sql`desc`}`

                    const handler = sortHandlers[s.field]

                    const col = handler ? handler(dto).colExpression : undefined

                    return col ? sql`${col} ${mode}` : undefined


            }))
            .with(sql`,`)
            .forClause(sql`order by`)
            .done()
            
            const joinClause = chain([
                
                ...typedConditions.map(c => conditionHandlers[c.type]?.(c).joinFragment),
                ...sort.map(s => sortHandlers[s.field]?.(dto).joinFragment)        
        
            ])
            .with(sql` `)
            .orFallbackTo(sql``)
            .done()
           
            const query = sql`
                select r.${sql(jsoncol)} 
                from ${sql(table)} as r 
                ${joinClause}
                ${whereClause}
                ${orderByClause}`



            console.log(await sql`${query}`.describe())  // must be wrapped as it "consumes" the query.

            // calculate and return total of matches, unless clients show they know it already.
            let total = dto.total

            if (!total)
                total = (await sql`${query}`).count ?? 0    // must be wrapped as it "consumes" the query.


            // page query

            const { limit, offset  } = normalise(dto.cursor)

            const pagequery = sql`
                with query as (${query})
                select * from query 
                offset ${offset} limit ${limit}`


            const results = (await pagequery.execute()).map(r => r[jsoncol])

            const batch: ResultBatch = { total, results }

            return batch
        }

    }


    return self

}