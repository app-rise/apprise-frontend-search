import { useT } from 'apprise-frontend-core/intl/language'
import { CategoryReference } from 'apprise-frontend-tags/category/model'
import { TagLabel } from 'apprise-frontend-tags/tag/label'
import { useTagModel } from 'apprise-frontend-tags/tag/model'
import { useTagStore } from 'apprise-frontend-tags/tag/store'
import { useTagUtils } from 'apprise-frontend-tags/tag/utils'
import { Wide } from 'apprise-ui/component/model'
import { DateBox } from 'apprise-ui/date/datebox'
import { NumberBox } from 'apprise-ui/numberbox/numberbox'
import { MultiSelectBox, SelectBox } from 'apprise-ui/selectbox/selectbox'
import { TextBox } from 'apprise-ui/textbox/textbox'
import { TokenBox } from 'apprise-ui/tokenbox/tokenbox'
import { ReactElement, ReactNode } from 'react'
import { ConditionType, SearchConditionData } from './model'

import "./queryfield.scss"
import { utils } from 'apprise-frontend-core/utils/common'

// the interface of input fields that collect values for search conditions.

export type QueryFieldProps<T extends SearchConditionData=SearchConditionData> = Wide & Partial<{

    condition: ConditionType                // may be used by generic fields to customise conditions.

    label: ReactNode                        // limited presentation control.
    onChange: (t:T | undefined) => void
    children: T
}>



// text field

export const TextField = (props: QueryFieldProps) => {

    const { children, condition= utils().mint(), label, onChange } = props

    return <TextBox minWidth={200} label={label} horizontal onChange={value => onChange?.(value ? { [condition]: value } : undefined)}>
        {children?.[condition]}
    </TextBox>

}

// tokenized field

export const TokenizedField = (props: QueryFieldProps) => {

    const { children, condition= utils().mint(), label, onChange } = props

    return <TokenBox minWidth={200} label={label} horizontal noClear onChange={values => onChange?.(values ? { [condition]: values } : undefined)}>
        {children?.[condition]}
    </TokenBox>

}

export const TypedTokenizedField = (props: QueryFieldProps) => {

    const { children, condition= utils().mint(), label, onChange } = props

    return <TokenBox minWidth={200} label={label} horizontal noClear onChange={values => onChange?.(values ? { type: condition, values } : undefined)}>
        {children?.[condition]}
    </TokenBox>

}

export const TagField = (props: QueryFieldProps & { mode?: 'multi' |  'single', category: CategoryReference, labelPrototype?: ReactElement  }) => {

    const { mode='multi', label,  children, condition= utils().mint(), category, onChange, labelPrototype, ...rest } = props
    const model = useTagModel()

    const displayMode = labelPrototype?.props.displayMode
    const width = displayMode === 'code' ? 100 : 150

    const tags = useTagStore().allTagsOfCategory(category).sort(displayMode==='code' || displayMode==='full'? model.codeComparator : model.comparator)
    const textOf = useTagUtils().nameOf

    const Box = mode==='multi' ?  MultiSelectBox : SelectBox

   
    return <Box label={label} minWidth={width}  horizontal noClear

        options={tags.map(t => t.id)}
        render={t => <TagLabel  noLink noIcon displayMode={displayMode} tag={t} {...labelPrototype?.props} />}
        textOf={textOf}
        onChange={refs => onChange?.(refs ? { [condition]: refs } : undefined)}
        
        {...rest}

        >

        {children?.[condition]}
    </Box>

}

export const TypedTagField = (props: QueryFieldProps & { mode?: 'multi' |  'single', category: CategoryReference, labelPrototype?: ReactElement  }) => {

    const { mode='multi', label,  children, condition= utils().mint(), category, onChange, labelPrototype, ...rest } = props
    const model = useTagModel()

    const displayMode = labelPrototype?.props.displayMode
    const width = displayMode === 'code' ? 100 : 150

    const tags = useTagStore().allTagsOfCategory(category).sort(displayMode==='code' || displayMode==='full'? model.codeComparator : model.comparator)
    const textOf = useTagUtils().nameOf

    const Box = mode==='multi' ?  MultiSelectBox : SelectBox

   
    return <Box label={label} minWidth={width}  horizontal noClear

        options={tags.map(t => t.id)}
        render={t => <TagLabel  noLink noIcon displayMode={displayMode} tag={t} {...labelPrototype?.props} />}
        textOf={textOf}
        onChange={refs => onChange?.(refs ? { type: condition, values: refs } : undefined)}
        
        {...rest}

        >

        {children?.values}
    </Box>

}

export const NumericField = (props: QueryFieldProps<Partial<{ [key: string] : {min?: number, max?: number}}>> & {

    minPrefix?: string
    maxPrefix?: string
    prototype?: ReactElement

}) => {

    const t = useT()

    const { children={}, prototype, onChange, condition='none', label, minPrefix = t('search.pill_min') , maxPrefix =  t('search.pill_max')} = props

    // change min/max with current spec, if we have still a value to preserve overall.
    const changeMin = (min: number | undefined) => onChange?.(min!=undefined || children[condition]?.max!=undefined ? { [condition]: { ...children[condition], min: min ?? undefined } } : undefined)
    const changeMax = (max: number | undefined) => onChange?.(max!=undefined || children[condition]?.min!=undefined ? { [condition]: { ...children[condition], max: max ?? undefined } } : undefined)

    return <div className='boundedfield'>

        <NumberBox  label={label} horizontal minWidth={160} prefix={<span className='prefix'>{minPrefix}</span>} noControls onChange={changeMin} {...prototype?.props}>
            {children[condition]?.min}
        </NumberBox>
        <NumberBox minWidth={160} prefix={<span className='prefix'>{maxPrefix}</span>} noControls onChange={changeMax} {...prototype?.props}>
            {children[condition]?.max}
        </NumberBox>

    </div>

}


export const DateField = (props: QueryFieldProps<Partial<{ [key: string] : {from?: string, to?: string}}>>) => {

    const t = useT()

    const { children={}, onChange, condition='none', label} = props

    // change min/max with current spec, if we have still a value to preserve overall.
    const changeFrom = (from: string | undefined) => onChange?.(from || children[condition]?.to ? { [condition]: { ...children[condition], from: from ?? undefined } } : undefined)
    const changeTo = (to: string | undefined) => onChange?.(to || children[condition]?.from ? { [condition]: { ...children[condition], to: to ?? undefined } } : undefined)

    return <div className='boundedfield'>

            <DateBox placeholder={t('search.pill_from')} width={130} label={label} horizontal onChange={changeFrom}>
                {children[condition]?.from}
            </DateBox>
            <DateBox placeholder={t('search.pill_to')} width={130} onChange={changeTo}>
                {children[condition]?.to}
            </DateBox>
     
    </div>

}

{/* <DateBox label={label} horizontal onChange={date => onChange?.(date ? { date } : undefined)}>
        {children?.date}
    </DateBox> */}