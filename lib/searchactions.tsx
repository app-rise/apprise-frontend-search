
import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { Button, ButtonProps } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { SidebarContext } from 'apprise-ui/page/sidebar'
import { SaveIconFull } from 'apprise-ui/utils/icons'
import { useContext } from 'react'
import { AiFillDelete, AiFillFolderOpen } from 'react-icons/ai'
import { BiUndo } from 'react-icons/bi'
import { IoIosPlay } from 'react-icons/io'
import { useSearchContext } from './boundary'
import { useCurrentQuery } from './context'
import { SearchRequest } from './model'
import { useSavedQuery } from './savequery'
import { useSearchEffects } from './searcheffects'
import { DeepPartial } from 'vite-plugin-checker/dist/esm/types'



export const useSearchActions = () => {

    const state = useSearchContext()

    const effects = useSearchEffects()

    const { edited, reset } = useCurrentQuery()

    const self = {

        // runs current query, optionally with just-in-time changes. 
        // if successful, "rebase" form to it, so we can reset it later (cf. revert action)
        runQuery: async (jitChanges:DeepPartial<SearchRequest>={}) => {

            const request = utils().merge({query:edited}, jitChanges)
           
            await effects.runSearch( request )

            reset.to(request.query).quietly()

        }

        ,

        revertQuery: () => reset.toInitial.quietly()

        ,

        resetQuery: () => {

            // we force a state change in case the query is already the start query we still want the layout to reset.
            state.set(s => s.query = { ...s.startQuery })

        }



    }

    return self
}


export const SearchButton = (props: ButtonProps) => {

    const t = useT()

    const actions = useSearchActions()

    const { dirty } = useCurrentQuery()

    const { className, ...rest } = props

    const classes = classname('run-btn', dirty && 'btn-highlight', className)

    return <Button noLabel className={classes} icon={<IoIosPlay style={{ marginLeft: 3 }} size={20} />}
        tip={t('search.run_btn_tip')} tipPlacement='bottom' tipDelay={.7}
        type='ghost'
        onClick={()=> actions.runQuery()}
        {...rest} />

}

export const QueryRevertButton = (props: ButtonProps) => {

    const t = useT()

    const actions = useSearchActions()

    const { dirty } = useCurrentQuery()

    const { className, ...rest } = props

    const classes = classname('undo-btn', className)

    return <Button enabled={dirty} noLabel className={classes} icon={<BiUndo color={dirty ? 'lightseagreen' : 'inherit'} />}
        tip={t('search.revert_btn_tip')} tipPlacement='bottom' tipDelay={.7}
        type='ghost'
        onClick={actions.revertQuery}
        {...rest} />

}


export const QueryResetButton = (props: ButtonProps) => {

    const t = useT()

    const actions = useSearchActions()

    const { className, ...rest } = props

    const classes = classname('revert-btn', className)

    return <Button noLabel className={classes} icon={<AiFillDelete color='lightseagreen' />}
        tip={t('search.reset_btn_tip')} tipPlacement='bottom' tipDelay={.7}
        type='ghost'
        onClick={actions.resetQuery}
        {...rest} />

}


export const OpenSidebarButton = (props: ButtonProps & {

    startOpen?: boolean

}) => {

    const t = useT()

    const { startOpen } = props

    const sidebar = useContext(SidebarContext)

    const { className, ...rest } = props

    const classes = classname('opensidebar-btn', className)

    const collapsed = sidebar.get().collapsed ?? !startOpen

    const toggleSidebar = () => {

        sidebar.set(s => s.collapsed = !collapsed)
    }

    return <Button noLabel className={classes} icon={<AiFillFolderOpen style={{ marginLeft: 3 }} color={collapsed ? 'dodgerblue' : 'hotpink'} />}
        tip={t('search.open_btn_tip')} tipPlacement='bottom' tipDelay={.7}
        type='ghost'
        onClick={toggleSidebar}
        {...rest} />

}


export const SaveQueryButton = (props: ButtonProps & {


}) => {

    const t = useT()

    const { className, ...rest } = props

    const { dirty, edited } = useCurrentQuery()

    const drawer = useSavedQuery()

    const classes = classname('revert-btn', className)

    const saveQuery = () => drawer.openWith(edited)

    return <Button disabled={dirty} noLabel className={classes} icon={<SaveIconFull color={dirty ? 'inherit' : 'dodgerblue'} />}
        tip={t('search.save_btn_tip')} tipPlacement='bottom' tipDelay={.7}
        type='ghost'
        onClick={(saveQuery)}
        {...rest} />

}

